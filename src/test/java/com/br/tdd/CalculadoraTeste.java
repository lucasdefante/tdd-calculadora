package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    @Test
    public void  testarSomaDeDoisNumeros(){
        int resultado = Calculadora.soma(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarSubtracaoDeDoisNumeros(){
        int resultado = Calculadora.subtracao(5,5);
        Assertions.assertEquals(0,resultado);
    }

    @Test
    public void testarSubtracaoDeDoisNumerosDecimais(){
        double resultado = Calculadora.subtracao(5.4, 5.2);
        Assertions.assertEquals(0.2, resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumeros(){
        int resultado = Calculadora.multiplicacao(4,4);
        Assertions.assertEquals(16, resultado);
    }

    @Test
    public void testarMultiplicacaoDeDoisNumerosDecimais(){
        double resultado = Calculadora.multiplicacao(0.51, 3);
        Assertions.assertEquals(1.53, resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumeros(){
        int resultado = Calculadora.divisao(49, 7);
        Assertions.assertEquals(7, resultado);
    }

    @Test
    public void testarDivisaoDeDoisNumerosDecimais(){
        double resultado = Calculadora.divisao(1, 4.0);
        Assertions.assertEquals(0.25, resultado);
    }
}
