package com.br.tdd;

public class Conta {

    private Cliente cliente;
    private double saldo;

    public Conta(Cliente cliente, double saldo) {
        this.cliente = cliente;
        this.saldo = saldo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void depositar(double valor){
        valor = valor - (valor * 0.2);
        this.saldo += valor;
    }

    public void sacar(double valor){
        if(valor > this.saldo){
            throw new RuntimeException("Saldo insuficiente");
        }
        this.saldo -= valor;
    }
}
