package com.br.tdd;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculadora {

    private static final int precisao = 2;

    public static int soma(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public static int subtracao(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero - segundoNumero;
        return resultado;
    }

    public static double subtracao(double primeiroNumero, double segundoNumero) {
        BigDecimal resultado = truncarDouble(primeiroNumero).subtract(truncarDouble(segundoNumero));
        return resultado.doubleValue();
    }

    public static int multiplicacao(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    public static double multiplicacao(double primeiroNumero, double segundoNumero) {
        BigDecimal resultado = truncarDouble(primeiroNumero).multiply(truncarDouble(segundoNumero));
        return resultado.doubleValue();
    }

    public static int divisao(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero / segundoNumero;
        return resultado;
    }

    public static double divisao(double primeiroNumero, double segundoNumero) {
        BigDecimal resultado = truncarDouble(primeiroNumero).divide(truncarDouble(segundoNumero));
        return resultado.doubleValue();
    }

    public static BigDecimal truncarDouble(double valor){
        BigDecimal bigDecimal = BigDecimal.valueOf(valor);
        bigDecimal = bigDecimal.setScale(precisao, RoundingMode.FLOOR);
        return bigDecimal;
    }
}
